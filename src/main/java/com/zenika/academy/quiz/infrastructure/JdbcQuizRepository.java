package com.zenika.academy.quiz.infrastructure;

import com.zenika.academy.quiz.domain.Quiz;
import com.zenika.academy.quiz.repositories.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class JdbcQuizRepository implements QuizRepository {

    private JdbcTemplate jdbcTemplate;
    private final RowMapper<Quiz> quizRowMapper = (rs, rowNum) -> new Quiz(rs.getString("id"), rs.getString("name"));

    @Autowired
    public JdbcQuizRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Quiz> getAll() {
        return this.jdbcTemplate.query("select quizzes.id, quizzes.name from quizzes", this.quizRowMapper);
    }

    @Override
    public void save(Quiz q) {
        this.jdbcTemplate.update("insert into quizzes (id, name) values (?, ?)", q.getId(), q.getName());
    }

    @Override
    public Optional<Quiz> getOne(String id) {
        try {
            final Quiz quiz = this.jdbcTemplate.queryForObject("select quizzes.id, quizzes.name \n" +
                    "from quizzes \n" +
                    "where quizzes.id = ?", this.quizRowMapper, id);
            return Optional.ofNullable(quiz);
        }
        catch (IncorrectResultSizeDataAccessException e) {
            return Optional.empty();
        }
    }
}
