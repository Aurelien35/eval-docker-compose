package com.zenika.academy.quiz.infrastructure;

import com.zenika.academy.quiz.domain.questions.MultipleChoiceQuestion;
import com.zenika.academy.quiz.domain.questions.OpenQuestion;
import com.zenika.academy.quiz.domain.questions.Question;
import com.zenika.academy.quiz.domain.questions.TrueFalseQuestion;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class QuestionExtractor {

    /**
     * Method used for a ResultSetExtractor that builds a list of questions.
     * It will advance the result set to the end.
     */
    public List<Question> buildSeveralQuestions(ResultSet rs) throws SQLException {
        List<Question> result = new ArrayList<>();
        if (rs.next()) {
            while (!rs.isAfterLast()) {
                result.add(this.buildOneInternal(rs));
            }
        }
        return result;
    }

    /**
     * Method used for a ResultSetExtractor that build one question.
     * It will advance the result set to the end.
     */
    public Question buildOneQuestion(ResultSet rs) throws SQLException {

        if (rs.next()) {
            return buildOneInternal(rs);
        }
        else {
            return null;
        }
    }

    /**
     * B uild one question.
     * The ResultSet MUST be on the first row of the question.
     * This method doesn't handle empty result sets.
     */
    private Question buildOneInternal(ResultSet rs) throws SQLException {
        final Question result;
        String currentQuestionId = rs.getString("id");

        // Case where the question is a TrueFalseQuestion
        if (rs.getString("true_false_answer") != null) {
            result = new TrueFalseQuestion(rs.getString("id"), rs.getString("question_text"), rs.getBoolean("true_false_answer"));
            rs.next();
            if (!rs.isAfterLast() && rs.getString("id").equals(currentQuestionId)) {
                throw new IllegalStateException("The question with id " + currentQuestionId + " has a boolean answer AND several suggestions");
            }
        } else if (rs.getString("open_answer") != null) {
            // No suggestion means it's an open question
            if (rs.getString("suggestion_text") == null) {
                result = new OpenQuestion(rs.getString("id"), rs.getString("question_text"), rs.getString("open_answer"));
                rs.next();
                if (!rs.isAfterLast() && rs.getString("id").equals(currentQuestionId)) {
                    throw new IllegalStateException("The question with id " + currentQuestionId + " has several suggestions but one of them is null");
                }
            }
            // multiple choice question : we need to iterate on the suggestions
            else {
                String questionText = rs.getString("question_text");
                String correctAnswer = rs.getString("open_answer");
                List<String> incorrectSuggestions = new ArrayList<>();
                do {
                    incorrectSuggestions.add(rs.getString("suggestion_text"));
                    rs.next();
                } while (!rs.isAfterLast() && rs.getString("id").equals(currentQuestionId));
                result = new MultipleChoiceQuestion(currentQuestionId, questionText, incorrectSuggestions, correctAnswer, new Random());
            }
        } else {
            throw new IllegalStateException("the question with id " + currentQuestionId + "has no answer.");
        }

        return result;
    }
}
