package com.zenika.academy.quiz.services;

import com.zenika.academy.quiz.controllers.representation.NewQuizRepresentation;
import com.zenika.academy.quiz.controllers.representation.QuizRepresentation;
import com.zenika.academy.quiz.domain.Quiz;
import com.zenika.academy.quiz.domain.questions.Question;
import com.zenika.academy.quiz.repositories.QuestionRepository;
import com.zenika.academy.quiz.repositories.QuizRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class QuizService {
    private QuizRepository quizRepository;
    private QuestionRepository questionRepository;

    @Autowired
    public QuizService(QuizRepository quizRepository, QuestionRepository questionRepository) {
        this.quizRepository = quizRepository;
        this.questionRepository = questionRepository;
    }

    public List<Quiz> getAll() {
        return this.quizRepository.getAll();
    }

    public List<Question> getQuestionsOfQuiz(String quizId) {
        return this.questionRepository.getByQuizId(quizId);
    }

    public Quiz createQuiz(NewQuizRepresentation body) {
        Quiz createdQuiz = new Quiz(UUID.randomUUID().toString(), body.getName());

        this.quizRepository.save(createdQuiz);
        return createdQuiz;
    }
}
